# 1. Checkout code from bitbucket
Create and go to working directory

1. *`git pull https://sandip_more@bitbucket.org/sandip_more/xseed_flask_code.git`*

2. *`git status`*

# 2. Create virtual environment from requirement.txt file.
1. Create virtual enrironment *`python -m venv VENV`*

2. Activate virtual enrironment *`.\VENV\Scripts\activate`*

3. Install packages mentioned in requirements.txt file *`pip install -r requirements.txt`*

# 3. Deployment on flask.
1. Open *`Deployment_Script.bat`* file.

2. In 2nd & 3rd line change experiment no and corresponding model no(e.g *`Experiment_1\1`*)

3. Once your model no is changed then open command prompt with current working directory *`Deployment_Script.bat`* then hit enter 

4. Once your work is done and you want to close the server then type *`cntrl+c`*
It will prompt to close session then type *`N`*