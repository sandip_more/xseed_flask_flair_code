# coding=utf-8
import fitz
# import docx2txt
from  yourapp.mydocx2txt import mydocx2txt
import string

from base64 import b64decode

def bytesBase64toText(base64EncodedData,docType):
    if(docType==".pdf"):
        txt=pdfToText(base64EncodedData)
        return txt
    if(docType==".docx"):
        txt=docxToText(base64EncodedData)
        return txt
    if(docType==".doc"):
        pass
def docxToText(base64EncodedData):
    txt=None
    with open('sb-sbtest.docx', 'wb') as fout:
        try:
            fout.write(b64decode(base64EncodedData))
        except Exception as inst:
            print("\nError in writing docx")
            print(inst)
            print("\n")
    try:
        txt=mydocx2txt('sb-sbtest.docx')
    except Exception as inst:
        print("\nError in mydocx2txt")
        print(inst)
        print("\n")
    
    # print(f"{type(txt)} {repr(txt)}")
    return txt

def pdfToText(base64EncodedData):
    txt=None
    
    try:
        with fitz.open(stream=b64decode(base64EncodedData), filetype='pdf') as doc:
            text = ""
            for page in doc:
                text = text + str(page.getText())
            txt = ''.join(x for x in text if x in string.printable)    
    except Exception as inst:
        print("\nError in reading  pdf from bytes")
        print(inst)
        #print(len(b))
        #print(base64EncodedData.endswith("="))
        print("\n")
    # print(txt)
    return txt


if __name__ == "__main__":
    import base64
    import os
    os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle")
    image = open('Abiral_Pandey_Fullstack_Java.docx', 'rb') #open binary file in read mode
    image_read = image.read()
    image_64_encode = base64.encodebytes(image_read)
    print(image_64_encode)

